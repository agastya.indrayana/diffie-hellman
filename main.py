from utilities import (generate_prime_with_primitive_root,
                       generate_random_number)
import json
from datetime import datetime

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

app = FastAPI()

origins = ["*"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/")
async def root():
    return {"message": "Hello World"}


@app.get("/get-prime")
async def get_prime(digits: int = 20):
    prime, root = generate_prime_with_primitive_root(digits)
    s = datetime.now()
    result = {
        "digits": digits,
        "prime": str(prime),
        "root": root,
        "process_time": datetime.now()-s
    }
    return result


@app.get("/get-random-number")
async def get_random_number(max_val: str, digits: int = 20):
    max_val = int(max_val)
    num = generate_random_number(digits, max_val)
    s = datetime.now()
    result = {
        "max_val": max_val,
        "digits": digits,
        "num": str(num),
        "process_time": datetime.now()-s
    }
    return result


@app.get("/mod-exponent")
async def mod_exponent(base: str, exp: str, mod: str):
    base, exp, mod = int(base), int(exp), int(mod)
    s = datetime.now()
    result = {
        "result": str(pow(base, exp, mod)),
        "base": str(base),
        "exp": str(exp),
        "mod": str(mod),
        "process_time": datetime.now()-s
    }
    return result
