import json
import random
from sympy import isprime, primitive_root
import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

formatter = logging.Formatter('%(asctime)s:%(name)s:%(levelname)s:%(message)s')

file_handler = logging.FileHandler('logfile.log')
file_handler.setFormatter(formatter)

logger.addHandler(file_handler)


def generate_prime_with_primitive_root(digits):
    it = 0
    while True:
        it += 1
        # Generate a random prime number with the specified number of digits
        num = random.randint(10 ** (digits - 1), 10 ** digits - 1)
        if isprime(num):
            root = primitive_root(num)
            if root is not None:
                logger.debug(json.dumps(
                    {"digits": digits, "num": num, "root": root, "it": it}, indent=2))
                return num, root


def generate_random_number(digits, max_val=None):
    if max_val < 10 ** (digits - 1):
        return 0
    while True:
        # Generate a random prime number with the specified number of digits
        num = random.randint(10 ** (digits - 1), 10 ** digits - 1)
        if num < max_val:
            return num
